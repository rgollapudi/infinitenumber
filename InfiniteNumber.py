class InfiniteNumber:

	# default constructor
	def __init__(self, optional_array=[], optional_string=""):
		self.digits = []

		if len(optional_array) > 0:
			# optional parameter has been provided
			self.digits = optional_array
		elif len(optional_string) != 0:
			length = len(optional_string)
			for index in range(length):
				self.digits.append(int(optional_string[length - 1 - index]))
		else:
			# read user input, no optional parameter provided
			while True:
				user_input = input(
					"Enter a digit and press enter:" 
					if len(self.digits) == 0 
					else "Enter another number or press enter to end input.")
				print(user_input)
				
				try:
					if len(user_input) == 1:
						self.digits.append(int(user_input[0]))
					elif len(user_input) > 1:
						print("You have not entered a digit, try again.")
						pass
					else:
						if len(self.digits) == 0:
							print("Insufficient digits, please try again.")
						else:
							break
				except:
					print("You have not entered a digit, try again.")
					pass
		
	def display(self):
		print_string = ""
		for i in range(len(self.digits)):
			print_string += str(self.digits[i])
		print(print_string)

	def add(self, second_num):
		# finish this function to return a InfiniteNumber by adding self to second_num
		def changetoint(input):
			''' It is used to convert the given input to integer
      Arguments:
        input : list of integers
      Returns:
        returns the integer'''
			return int("".join(map(str, input)))

		def changetolist(integer):
			''' It used to convert the given input to integer
      Arguments:
          input : integer
      Returns:
          returns the list of integers'''
			return [int(x) for x in str(integer)]
		return changetolist(changetoint(self.digits) + changetoint(second_num))
		
	def increment(self):
		a = self.digits
		n = len(a)

		# finish this function to return a InfiniteNumber by incrementing the current number
		# you can use the add() function if you want
		
	def compare(self, second_num):
		# finish this function to 
		# 	return 	1	if second_num is smaller than self
		#	return -1 if second_num is larger than self
		#	return 	0	if second number is equal to self
		a = self.digits
		b = second_num
		# finish this function to
		# 	return 	1	if second_num is smaller than self
		#	return -1 if second_num is larger than self
		#	return 	0	if second number is equal to self
		if len(a) > len(b):
			return 1
		elif len(b) > len(a):
			return -1
		else:
			for num in range(len(a)):
				if a[num] > b[num]:
					return 1
				elif b[num] > a[num]:
					return -1
			return 0

	def mul(self, second_num):
		# finish this function to return a InfiniteNumber by multiplying self with
		#	the current number.
		# you can use the add(), increment() functions if you want
		def changetoint(input):
			return int("".join(map(str, input)))

		def changetolist(integer):
			return [int(x) for x in str(integer)]
		return changetolist(changetoint(self.digits) * changetoint(second_num))

	def pow(self, second_num):
		# finish this function to return a InfiniteNumber
		# you can use the mul(), increment() functions if you want
		def changetoint(input):
			return int("".join(map(str, input)))
		def changetolist(integer):
			return [int(x) for x in str(integer)]
		res = 1
		num1 = changetoint(self.digits)
		num2 = changetoint(second_num)
		for i in range(num2):
			res *= num1
		return changetolist(res)

first = InfiniteNumber([1,2,3])
second = InfiniteNumber([3,2,1])
print(first.digits)
print(second.digits)
number = first.add(second.digits)
first1 = first.increment()
first2 = first.compare(second.digits)
first3 = first.mul(second.digits)
first4 = first.pow(second.digits)
print(number)
print(first1)
print(first2)
print(first3)
print(first4)
